
import requests
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from PIL import Image
from io import BytesIO
import time, json
import io


subscription_key = "78ed0f939da6428d89f4b1343378c20f"

assert subscription_key


vision_base_url = "https://westeurope.api.cognitive.microsoft.com/vision/v2.0/"

ocr_url = vision_base_url + "recognizeText"


image_url = "https://bitbucket.org/miguel_acevedo_ve/python-stream/raw/086279ad6885a490e521785ba288914ed98cfd1d/test.jpg"

image_path = "test.jpg"

imageObject  = Image.open("test.jpg")
cropped     = imageObject.crop((490,400,650,600))
cropped.show()
#cropped     = imageObject.crop((190,400,400,600))
#print(type(cropped))


imgByteArr = io.BytesIO()
cropped.save(imgByteArr, format='JPEG')
imgByteArr = imgByteArr.getvalue()

image_data = open(image_path, "rb").read()
print(type(image_data))

headers = {'Content-Type': 'application/octet-stream','Ocp-Apim-Subscription-Key': subscription_key}
params  = {'mode' : 'Printed','language':'unk'}
data    = {'url': image_url}
#response = requests.post(ocr_url, headers=headers, params=params, json=data)
#cropped.show()
response = requests.post(
    ocr_url, headers=headers,params=params, data=imgByteArr)
#print(response)
operationLocation = response.headers['Operation-Location']
print(operationLocation)
time.sleep(2)
response = requests.request('GET', operationLocation, json=None, data=None, headers=headers, params=None)

parsed = json.loads(response.text)
#print(parsed)
lines = parsed['recognitionResult']['lines']

for line in lines:  
    print (line['text'])
